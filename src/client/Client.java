package client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import datamodel.GPSPoint;
import datamodel.Location;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Program that simulates a device with GPS that sends its location.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
public class Client implements Runnable {

	private String serverAddress;
	private int port;
	private String queueUsername;
	private String queuePassword;
	/**
	 * The value is about 5 meters in coordinate system.
	 */
	final private double maxOffsetCoords = 0.000045;
	final private int maxOffsetMeters = 3;
	/**
	 * Distance is in meters
	 */
	final private int rasterSize = 1;
	private ArrayList<UUID> ids;
	private GPSPoint sw;
	private GPSPoint ne;
	private GPSPoint clientLocation;
	private final static String QUEUE_NAME = "GPSLocation";
	private ConnectionFactory factory;
	private AMQP.BasicProperties.Builder builder;
	private static Long sleepPeriod = new Long(1 * 1000);
//	private Connection connection;
//	private Channel channel;

	/**
	 * Constructor that generates a random location on a 50 by 50 grid.
	 */
	public Client() {
		this(1);
	}

	public Client(int amountClients) {
		this(GPSPoint.getRandomLocationInArea(new GPSPoint(0, 0), new GPSPoint(50, 50)), new GPSPoint(-180, -180), new GPSPoint(180, 180), amountClients);
		System.out.println("amountClients:" + amountClients);
	}

	/**
	 * Constructor which gives the most control and invokes the initialization.
	 *
	 * @param clientLocation location of the client at the moment. May not be
	 * null
	 * @param southWest Southwest corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param northEast Northeast corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param amountClients The amount of clients to be simulated
	 */
	public Client(GPSPoint clientLocation, GPSPoint southWest, GPSPoint northEast, int amountClients) {
		initializeNetwork(amountClients);
		initializeLocations(clientLocation, southWest, northEast);
		initializeRabbitMQ();
	}

	private void initializeNetwork(int amountClients) {
		Properties props = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("host.properties");
			props.load(fis);

			serverAddress = props.getProperty("SERVER_ADDRESS");
			port = Integer.parseInt(props.getProperty("PORT"));

			queueUsername = props.getProperty("QUEUE_USERNAME");
			queuePassword = props.getProperty("QUEUE_PASSWORD");
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
		}

		ids = new ArrayList<>();
		for (int i = 0; i < amountClients; ++i) {
			ids.add(UUID.randomUUID());
		}
	}

	/**
	 * Sets the attributes of the class.
	 *
	 * @param clientLocation Location where the client is.
	 * @param southWest Southwest corner of the area the client needs to stay in
	 * to send data to the server.
	 * @param northEast Northeast corner of the area the client needs to stay in
	 * to send data to the server.
	 */
	private void initializeLocations(GPSPoint clientLocation, GPSPoint southWest, GPSPoint NorthEast) {
		this.clientLocation = clientLocation;
		sw = southWest;
		ne = NorthEast;
	}

	private void initializeRabbitMQ() {
		factory = new ConnectionFactory();
		factory.setHost(serverAddress);
		factory.setUsername(queueUsername);
		factory.setPassword(queuePassword);
		builder = new AMQP.BasicProperties().builder();
		builder.type(QUEUE_NAME);
		builder.expiration(sleepPeriod.toString());
	}

	/**
	 * Main method for Client starts a client
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("args: " + args[0]);
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);

		if (args.length > 0) {
			try {
				int firstArg = Integer.parseInt(args[0]);
				System.out.println("firstArg" + firstArg);
				scheduledExecutorService.scheduleAtFixedRate(new Client(firstArg), 0, 60, TimeUnit.SECONDS);

			} catch (NumberFormatException e) {
				System.err.println("Argument" + args[0] + " must be an integer.");
				System.exit(1);
			}

		} else {
			new Client().run();
		}
	}

	/**
	 * Generates a Location object and sends it to the predetermined queue.
	 */
	private void sendData(UUID id) {
		System.out.println(id);
		try {
			Connection connection;
			Channel channel;
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			builder.timestamp(new Date());
			Location x = new Location(id, clientLocation.locationOnRaster(sw, rasterSize));

			channel.basicPublish("", QUEUE_NAME, builder.build(), x.getBytes());
			System.out.println(" [x] Sent '" + x + "'");

			System.out.println("Sent new location to queue");
			channel.close();
			connection.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Infinite loop with : - Check within the defined area, if not, sleep for
	 * an iteration - Connect to the server - Convert it's coordinates to an
	 * index on the raster for the heatmap - Send these coordinates to the
	 * server.
	 */
	@Override
	public void run() {
//		try {
//			connection = factory.newConnection();
//			channel = connection.createChannel();
//			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//			builder.timestamp(new Date());
		for (UUID id : ids) {
			clientLocation = clientLocation.getRandomLocationNear(maxOffsetMeters);

			if (clientLocation.onLocation(sw, ne)) {
				sendData(id);
			} else {
				String msg = "Client was not in area between " + sw + " and " + ne + " with location " + clientLocation;
			}
		}
//			channel.close();
//			connection.close();
//		} catch (IOException ex) {
//			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
//		}
	}
}
