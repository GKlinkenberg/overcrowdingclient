package datamodel;

import java.awt.Point;
import java.util.Random;

/**
 * Class that stores an x and y coordinate and can generate new coordinates in a
 * certain area.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
public class GPSPoint {

    public double x, y;

    /**
     * Constructor that sets the points
     *
     * @param x X coordinate of the Point
     * @param y X coordinate of the Point
     */
    public GPSPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Generates a random location in the area between sw and ne
     *
     * @param sw Southwest corner of the area the client needs to stay in to
     * send data to the server.
     * @param ne Northeast corner of the area the client needs to stay in to
     * send data to the server.
     * @return
     */
    public static GPSPoint getRandomLocationInArea(GPSPoint sw, GPSPoint ne) {
        Random rand = new Random();
        double x = Math.min(sw.x, ne.x) + rand.nextDouble() * (Math.abs(ne.x - sw.x));
        double y = Math.min(sw.y, ne.y) + rand.nextDouble() * (Math.abs(ne.y - sw.y));
        return new GPSPoint(x, y);
    }

    /**
     * Generates a random location near the current location of the point.
     * Currently the data points will never be higher than 50 or lower than 50.
     *
     * @param maxOffsetMeters the maximum distance between the current point and
     * the new point.
     * @return The new random location near the previous location
     */
    public GPSPoint getRandomLocationNear(int maxOffsetMeters) {
        Random random = new Random();

        double diffX = (random.nextDouble() * 2 - 1) * maxOffsetMeters;
        double diffY = (random.nextDouble() * 2 - 1) * maxOffsetMeters;

        double newX = x + diffX;
        double newY = y + diffY;

        if (newX > 50 || newX < 0) {
            newX = random.nextInt(50);
        }

        if (newY > 50 || newY < 0) {
            newY = random.nextInt(50);
        }
        return new GPSPoint(newX, newY);
    }

    /**
     * Determines if the GPSPoint is in the area described by sw and ne
     *
     * @param sw The south west corner of the area
     * @param ne The north west corner of the area
     * @return true if GPSPoint is in the area, else return false.
     */
    public boolean onLocation(GPSPoint sw, GPSPoint ne) {
        return this.x >= Math.min(sw.x, ne.x) && this.x <= Math.max(sw.x, ne.x) && this.y >= Math.min(sw.y, ne.y) && this.y <= Math.max(sw.y, ne.y);
    }

    /**
     * Calculates the distance divided by a factor and is floored.
     *
     * @param value The distance that is given
     * @param factor size of grid
     * @param origin origin of area
     * @return (int) (value - origin) /factor
     */
    private int roundDown(double value, double factor, double origin) {
        double coord = (value - origin) / factor;
        Double res = Math.floor(coord);
        return res.intValue();
    } 

    /**
     * Generates the coordinates on the raster for the gpspoint.
     *
     * @param sw
     * @param rasterSize
     * @return
     */
    public GPSPoint locationOnRaster(GPSPoint sw, int rasterSize) {
//        return new GPSPoint(roundDown(this.x, rasterSize, sw.x), roundDown(this.y, rasterSize, sw.y));
		Random r = new Random();
		return new GPSPoint(53 + r.nextDouble(), 6 + r.nextDouble());
    }

    /**
     * @return the string representation of the location
     */
    @Override
    public String toString() {
        return "GPSPoint{" + "x=" + x + ", y=" + y + '}';
    }
}
